<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

// declare some useful stuff here
$SESSIONID;
$RATELIMITED = false;
$SUCS_LOGIN = false;
$ISS_LOGIN = false;

ini_set("session.use_strict_mode",1);

//php7.1 and above
ini_set("session.sid_bits_per_character",5);
ini_set("session.sid_length",64);
//php7.1 below
ini_set("session.hash_bits_per_character",5);
ini_set("session.hash_function", "sha256");

session_set_cookie_params(strtotime("+3 days"),"/","sucs.org",true,true);
session_name('sucs_sso_id_v1');

session_start();
$SESSIONID = session_id();

// look at the db to see if they have an entry, if so load data in vars for use
// otherwise set them up
require "../lib/db.php";

// Check if they have just cleared their cookie/session id and are trying to bypass the rate limiting
// best we can do is IP ratelimit people
$ipBan_result = $DB_CON->query("SELECT * FROM sessions WHERE ipaddr='${_SERVER["REMOTE_ADDR"]}' ORDER BY lastfailedlogintime DESC LIMIT 1");
$ipBan_details = $ipBan_result->fetchArray();

// if their last login attempt was less than 30 mins ago
// 30 mins to really punish ban avoiders
if ( $ipBan_details["id"] !== $SESSIONID && $ipBan_details["lastfailedlogintime"] >= strtotime("-30 minutes") ) {
	$RATELIMITED = true;
}

$result = $DB_CON->query("SELECT * FROM sessions WHERE id='${SESSIONID}'");

$details = $result->fetchArray();

// if there's an entry then load that data otherwise
// otherwise make an entry
if ( $details["id"] === $SESSIONID && !$RATELIMITED) {
	//var_dump($details);
	//echo time();
	if ($details["sucs_username"] !== null) {
		$SUCS_LOGIN = true;
	}
	if ($details["iss_username"] !== null) {
		$ISS_LOGIN = true;
	}
	// every 10 minutes give people not clearing their cookies 1 more go
	if ($details["failedlogincount"] >= 3 && $details["lastfailedlogintime"] <= strtotime("-10 minutes")) {
		$details["failedlogincount"] = 2;
	}
	if ($details["failedlogincount"] >= 3) {
		$RATELIMITED = true;
	}
} else {
	$DB_CON->exec("INSERT INTO sessions (id, ipaddr, failedlogincount) VALUES ('${SESSIONID}','${_SERVER["REMOTE_ADDR"]}',0)");
}

// look for username and password in $_POST first
// otherwise try using the legcay sucssite_session cookie/info
// if all that fails then throw up a login form
if ( isset($_POST["username"]) && isset($_POST["password"]) && !$RATELIMITED ) {

	// require the login lib and validate the user/password
	require("../lib/ldap-auth/ldap-auth.php");
	$isAuthd = ldapAuth($_POST["username"], $_POST["password"]);

	$username = strtolower($_POST["username"]);

	if ($isAuthd == "sucs"){
		//do stuff for sucs auth
		$DB_CON->exec("UPDATE sessions SET sucs_username='${username}' WHERE id='${SESSIONID}'");
		$SUCS_LOGIN = true;
		$details["sucs_username"] = $username;

		if (isset($_GET["callbackapp"]) && isset($_GET["callbackpath"])) {
			header("Location: http://${_GET["callbackapp"]}.sucs.org/${_GET["callbackpath"]}");
		}

	} elseif ($isAuthd == "uni"){
		//do stuff for uni auth
		$DB_CON->exec("UPDATE sessions SET iss_username='${username}' WHERE id='${SESSIONID}'");
		$ISS_LOGIN = true;
		$details["iss_username"] = $username;
	}else{
		//do stuff for not authd peeps
		$details["failedlogincount"] = $details["failedlogincount"] + 1;
		$DB_CON->exec("UPDATE sessions SET failedlogincount=${details['failedlogincount']}, lastfailedlogintime=strftime('%s','now') WHERE id='${SESSIONID}'");
		if ($details["failedlogincount"] >= 3) {
			$RATELIMITED = true;
		}
	}

} elseif ( isset($_COOKIE["sucssite_session"]) && !$RATELIMITED) {

	// found a sucssite_session
	$legacySessionID = $_COOKIE["sucssite_session"];

	// connect to the sucssite db to get the username of the session
	$db_connection = pg_connect("dbname=sucssite");
	$username = pg_fetch_result(pg_query_params($db_connection, "SELECT * FROM session WHERE hash=$1", array($legacySessionID)), 0, "username");

	if ($username !== null && $username !== false && username !== "") {
		// we have a vlid username from a old session
		$DB_CON->exec("UPDATE sessions SET sucs_username='${username}' WHERE id='${SESSIONID}'");
		$SUCS_LOGIN = true;
		$details["sucs_username"] = $username;
		if (isset($_GET["callbackapp"]) && isset($_GET["callbackpath"])) {
			header("Location: http://${_GET["callbackapp"]}.sucs.org/${_GET["callbackpath"]}");
		}
	}

}

?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<title>SUCS sso</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://common.sucs.org/css/sucs.css">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</head>

	<body>

		<nav class="navbar navbar-light bg-light">
			<a class="navbar-brand" href="https://sucs.org"><img src="https://common.sucs.org/img/sucs_logo_white_trans.svg" /></a>
		</nav>

		<div class="h-100 container justify-content-center text-center">
			<div class="h-100 row d-flex justify-content-center align-items-center">
				<div class="col-8">
					<div class="card mb-4 mt-4">

<?php

// do something on failed login attempt
if (isset($isAuthd) && $isAuthd === "nope") {
	print('
		<div class="alert alert-danger" role="alert">
			❌ Login failed! 😞
		</div>
	');
}
if ($RATELIMITED) {
	print('
		<div class="card-body">
			<h4 class="card-title">👨‍⚖️</h4>
			<p>Too many inncorrect password attempts!. Sober up and try again in a bit</p>
		</div>
	');
} elseif (!$SUCS_LOGIN && !$ISS_LOGIN) {
	print('
		<div class="card-body">
			<h4 class="card-title">🔑</h4>
			<p class="card-text">Hello stranger! Try your SUCS Login</p>
				<form method="post">
					<div class="form-group">
						<input type="text" name="username" size="15" class="form-control" placeholder="Enter SUCS username" /><br />
						<input type="password" name="password" size="15" class="form-control" placeholder="Enter password" /><br />
						<input type="submit" class="btn btn-primary" value="SUCS Login" />
					</div>
				</form>
			</div>
		</div>
	<div class="card mb-4">
		<div class="card-body">
			<h4 class="card-title">🔑</h4>
			<p class="card-text">Hello stranger! Try your ISS Login</p>
				<form method="post">
					<div class="form-group">
						<input type="text" name="username" size="15" class="form-control" placeholder="Enter ISS username (student number)" /><br />
						<input type="password" name="password" size="15" class="form-control" placeholder="Enter password" /><br />
						<input type="submit" class="btn btn-primary" value="ISS Login" />
					</div>
				</form>
			</div>
	');
} elseif ($SUCS_LOGIN && !$ISS_LOGIN) {
	if ($details["sucs_username"] == null) {
		$sucs_username = $_POST["username"];
	} else {
		$sucs_username = $details["sucs_username"];
	}
	print('
		<div class="card-body">
			<h4 class="card-title">🔑 ISS</h4>
			<p class="card-text">Hello '.$sucs_username.'! You\'re logged in but may also login with your ISS details</p>
				<form method="post">
					<div class="form-group">
						<input type="text" name="username" size="15" class="form-control" placeholder="Enter ISS username" /><br />
						<input type="password" name="password" size="15" class="form-control" placeholder="Enter password" /><br />
						<input type="submit" class="btn btn-primary" value="ISS Login" />
					</div>
				</form>
			</div>
	');
} elseif (!$SUCS_LOGIN && $ISS_LOGIN) {
	if ($details["iss_username"] == null) {
		$iss_username = $_POST["username"];
	} else {
		$iss_username = $details["iss_username"];
	}

	print('
		<div class="card-body">
			<h4 class="card-title">🔑 SUCS</h4>
			<p class="card-text">Ah you\'ve only logged in with your ISS username: '.$iss_username.'</p>
				<form method="post">
					<div class="form-group">
						<input type="text" name="username" size="15" class="form-control" placeholder="Enter SUCS username" /><br />
						<input type="password" name="password" size="15" class="form-control" placeholder="Enter password" /><br />
						<input type="submit" class="btn btn-primary" value="SUCS Login" />
					</div>
				</form>
			</div>
	');
} else {
	print('
		<div class="card-body">
			<h4 class="card-title">✔️</h4>
			<p>Hi '.$details["sucs_username"].' you have logged into your ISS account '.$details["iss_username"].' already! 🍻</p>
		</div>
	');
}
?>

					</div>
				</div>
			</div>
		</div>

	</body>

</html>
