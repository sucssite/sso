<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*
* SUCS SSO API v1
* return some json verifiying people have logged in
* id=<value of sucs_sso_id_v1 cookie> REQUIRED
* iss=<true|false> OPTIONAL - return if they have logged in using iss creds (default false)
*
* apistate - should always be "ok" if not then don't trust the result
* sucs_username - will be their username
* iss_username - will be their iss username, only if requested
*/

function ip_in_range($ip, $range) {
	if (strpos($range, '/') == false) {
		$range .= '/32';
	}
	// $range is in IP/CIDR format eg 127.0.0.1/24
	list($range, $netmask) = explode('/', $range, 2);
	$ip_decimal = ip2long($ip);
	$range_decimal = ip2long($range);
	$wildcard_decimal = pow(2, (32 - $netmask)) - 1;
	$netmask_decimal = ~ $wildcard_decimal;
	return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
}

$ipAddr = $_SERVER['REMOTE_ADDR'];

if (!ip_in_range($ipAddr, "137.44.10.0/25")){
	exit("SSO API v1 not allowed outside of SUCSNET");
}

if ($_GET["id"] === null || !ctype_alnum($_GET["id"])) {
	// no id specified so bomb out
	$apidata = array("apistate" => "notok");
	print(json_encode($apidata));
	exit(1);
}

if ($_GET["id"] !== null && ctype_alnum($_GET["id"])) {
	// got a id and it looks valid-ish

	require("../../../lib/db.php");
	$result = $DB_CON->query("SELECT * FROM sessions WHERE id='${_GET["id"]}'");
	$details = $result->fetchArray();

	if (!$details) {
		// not a valid id so bomb out
		$apidata = array("apistate" => "notok");
		print(json_encode($apidata));
		exit(1);
	}

	$apidata = array("apistate" => "ok");

	$apidata = array_merge($apidata, array("sucs_username" => $details["sucs_username"]));

	if (isset($_GET["iss"]) && $_GET["iss"] === "true") {
		$apidata = array_merge($apidata, array("iss_username" => $details["iss_username"]));
	}

	print(json_encode($apidata));
	exit(0);
}


?>