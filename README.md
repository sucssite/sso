# SUCS Single Sign On

## Core

The core of the Application `public/index.php` allows people to verify their 
sucs and iss accounts with us.

### Callbacks

You can use `GET` Requests to call back to your apps. The params `callbackapp` 
and `callbackpath` are simple, they translate to `<callbackapp>.sucs.org/<callbackpath>`

```http://sucs.org/~imranh/sso/public?callbackapp=doorkey&callbackpath=/```


## API

### v1


Simple GET based API to return some json verifying people have logged in

id=<value of `sucs_sso_id_v1` cookie> REQUIRED

iss=<true|false> OPTIONAL - return if they have logged in using iss creds (default false)

---

apistate - should always be "ok" if not then don't trust the result

sucs_username - will be their username

iss_username - will be their iss username, only if requested

