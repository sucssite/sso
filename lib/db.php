<?php
// dynamic db path based one what part of the system imports the file
$cwd = explode("/",getcwd());
if ($cwd[count($cwd)-3] === "public" && $cwd[count($cwd)-2] === "api" && preg_match("/^v[0-9]$/",$cwd[count($cwd)-1])) {
	$DB_PATH = "../../../sso.db";

}
if ( $cwd[count($cwd)-1] === "public" ) {
	$DB_PATH = "../sso.db";
}

$DB_CON;
if ( !file_exists($DB_PATH) ) {
	$DB_CON = new SQLite3($DB_PATH);
	$DB_CON->exec("CREATE TABLE sessions
		(
			id TEXT PRIMARY KEY NOT NULL,
			sucs_username TEXT,
			iss_username TEXT,
			ipaddr TEXT NOT NULL,
			failedlogincount INT NOT NULL,
			lastfailedlogintime INT
		)"
	);
} else {
	$DB_CON = new SQLite3($DB_PATH);
}
?>